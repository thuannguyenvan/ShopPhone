﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Common.Models
{
    public class Tag : BaseModel
    {
        public string Name { get; set; }
        public string Link { get; set; }
    }
}
