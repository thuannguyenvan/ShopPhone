﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Common.Models
{
    public class SanPham : BaseModel
    {
        public string Ten { get; set; }
        public string TinhTrang { get; set; }
        public bool? ConHang { get; set; }
        public DateTime? NgayNhapHang { get; set; }
        public string Description { get; set; }
        public decimal? GiaBan { get; set; }
        public int? PhanTramSale { get; set; }
        public decimal? GiaNhap { get; set; }
        public int? Id_Category { get; set; }
        public string Url_Img { get; set; }
    }
}
