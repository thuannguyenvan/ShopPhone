﻿using Shop.Common.Models;
using Shop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Bussiness.Service
{
    public class TagService : GenericService<TAG>
    {
        public TagService(ShopEntities context) : base(context)
        {

        }
        public List<Tag> GetTags()
        {
            var result = new List<Tag>();
            foreach (var item in All.ToList())
            {
                result.Add(new Tag() {
                    Id=item.ID,
                    Name=item.Name,
                    Link=item.Link
                });
            }
            return result;
        }

        public void DeleteTag(int id)
        {
            Delete(id);
            Save();
        }

        public void AddTag(Tag tag)
        {
            InsertAndSave(new TAG() {
                Name=tag.Name,
                Link=tag.Link
            });
        }

        public void EditTag(Tag tag)
        {
            var tagOld = Find(tag.Id);
            tagOld.Name = tag.Name;
            tagOld.Link = tag.Link;
            UpdateAndSave(tagOld);
        }
    }
}
