﻿using Shop.Common.Models;
using Shop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Bussiness.Service
{
    public class SanPhamService : GenericService<SANPHAM>
    {
        public SanPhamService(ShopEntities context) : base(context)
        {

        }

        public List<SanPham> GetSanPhams()
        {
            var result = new List<SanPham>();
            foreach (var item in All.ToList())
            {
                result.Add(new SanPham()
                {
                    Id = item.Id,
                    ConHang = item.ConHang,
                    Description = item.Description,
                    GiaBan = item.GiaBan,
                    GiaNhap = item.GiaNhap,
                    Id_Category = item.Id_Category,
                    NgayNhapHang = item.NgayNhapHang,
                    PhanTramSale = item.PhanTramSale,
                    Ten = item.Ten,
                    TinhTrang = item.TinhTrang
                });
            }
            return result;
        }

        public void DeleteSanPham(int id)
        {
            Delete(id);
            Save();
        }

        public void AddSanPham(SanPham sanPham)
        {
            InsertAndSave(new SANPHAM()
            {
                Ten = sanPham.Ten,
                TinhTrang = sanPham.TinhTrang,
                PhanTramSale = sanPham.PhanTramSale,
                NgayNhapHang = sanPham.NgayNhapHang,
                Id_Category = sanPham.Id_Category,
                GiaNhap = sanPham.GiaNhap,
                ConHang = sanPham.ConHang,
                Description = sanPham.Description,
                GiaBan = sanPham.GiaBan,
                Url_Img = sanPham.Url_Img
            });
        }

        public void EditSanPham(SanPham sanPham)
        {
            var sp = Find(sanPham.Id);
            sp.Ten = sanPham.Ten;
            sp.TinhTrang = sanPham.TinhTrang;
            sp.PhanTramSale = sanPham.PhanTramSale;
            sp.NgayNhapHang = sanPham.NgayNhapHang;
            sp.Id_Category = sanPham.Id_Category;
            sp.GiaNhap = sanPham.GiaNhap;
            sp.ConHang = sanPham.ConHang;
            sp.Description = sanPham.Description;
            sp.GiaBan = sanPham.GiaBan;
            sp.Url_Img = sanPham.Url_Img;
            UpdateAndSave(sp);
        }
    }
}
