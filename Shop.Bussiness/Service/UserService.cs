﻿using Shop.Common.Models;
using Shop.Repository;
using System.Linq;

namespace Shop.Bussiness.Service
{
    public class UserService : GenericService<USER>
    {
        public UserService(ShopEntities context) : base(context)
        {
        }

        public User GetUserByUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                return null;
            var query = from c in All
                        orderby c.Id
                        where c.UserName == username
                        select c;
            var user = query.FirstOrDefault();
            return new User() {
                DiaChi=user.DiaChi,
                Email=user.Email,
                Id=user.Id,
                PassWord=user.PassWord,
                Phone=user.Phone,
                Role_Id=user.Role_Id,
                UserName=user.UserName
            };
        }

        public bool Login(string userName , string password)
        {
            var result = All.Where(x => x.UserName == userName).FirstOrDefault();
            return result.PassWord == password ? true : false;
        }


    }
}
